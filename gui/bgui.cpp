#include <string>

#include "../include/broker.hpp"
#include "./main.cpp"

// Setup the broker stuff
broker setup_broker(){
  gui::println("Broker setup");
  
  gui::print("Name: ");
  std::string name = gui::get_input();
  
  gui::print("Capital: ");
  int capital = std::stoi(gui::get_input());
  
  broker b(capital, name);
  
  gui::println("Broker setted up!");
  
  return b;
}

// Buy stock 
void buy_broker(broker &b){
  gui::println("Buy");
  
  gui::print("Amount Bought: ");
  int a = std::stoi(gui::get_input());
  
  gui::print("Stock: ");
  std::string s = gui::get_input();
  
  gui::println("Buying " + s + "for " + std::to_string(a));
    
  if (b.buy(s, a)) {
    gui::println("Success!");
  } else {
    gui::println("Failed!");
  }
}

// Sell
void sell_broker(broker &b){
  gui::println("Sell");
  
  gui::print("Stock: ");
  std::string s = gui::get_input();
  
  gui::print("Amount: ");
  int a = std::stoi(gui::get_input());
  
  gui::print("Amount to sell for: ");
  int t = std::stoi(gui::get_input());
  
  gui::println("Selling " +  s + "with quantity " + std::to_string(a) + "for " + std::to_string(t));
  
  if(b.sell(s, a, t)){
    gui::println("Success!");
  } else {
    gui::println("Failed!");
  }
}
