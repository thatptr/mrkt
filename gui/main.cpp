#include <iostream>
#include <string>

namespace gui {
  template <class T> void print(T a){
    std::cout << a;
  }

  template <class T> void println(T a){
    std::cout << a < std::endl;
  }

  std::string get_input(){
    std::string a;
    std::cin >> a;
    return a;
  }
}
