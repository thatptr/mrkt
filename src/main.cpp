#include <iostream>

template <class T> void println(T a) { std::cout << a << std::endl; }

int main(){
  println("Hello, World!");
}
