#pragma once

// Includes
#include <map>
#include <memory>
#include <numeric>
#include <string>
#include <vector>

// Local Includes
#include "./files.cpp"
#include "./math.cpp"

// Smarket class
class smarket {
private:
  // Capital: Amount of money in the stock market, exchanges: the exchenages in
  // the market
  int capital, exchanges;

  // Infaltion and trust refer to the stability of the market
  float inflation, trust;

  // Nmae of the exchange
  std::string name;

  // File to track the market activities
  std::unique_ptr<mrkt::files> a = std::make_unique<mrkt::files>();

  // Exchange housekeeping
  std::vector<std::string> exchange_vector;
  std::map<std::string, float> exchange_prices;
  std::map<std::string, float> prev_exchange_price;
  std::map<std::string, float> exchange_amount;
  std::map<std::string, int> exchange_total;
  std::map<std::string, std::vector<std::string>> exchange_users;

  // Update the market activities file
  void update(std::string info);

  // Setter method for exchange price
  bool set_exchange_price(std::string exchange_name, float new_price);

public:
  // Smarket Initialization class
  smarket(int capital, std::string name) {
    // Basics
    this->capital = capital;
    this->name = name;

    // This will come in later
    this->inflation = 1.0;
    this->trust = 10.0;

    // We start with to exchanges
    this->exchanges = 0;

    // Creating the files to store info
    a->create_folder("~/.local/mrkt/");
    a->create_file("~/.local/mrkt/" + this->name);

    this->update("Market " + this->name + " created");
  }
  // Creates an exchange
  void create_exchange(std::string exchange_name, float exchange_price,
                       int total_shares);

  // Getter method for exchanges
  int get_total_exchanges();

  // Get exchanges
  std::string *get_exchanges();

  // Getter method for exchange price
  float get_exchange_price(std::string exchange_name);

  // Getting exchange shares
  int get_exchange_shares(std::string exchange_name);

  // Changes the exchange price after someone bought something
  void buy(std::string name, std::string stock, int share_bought);

  // Changes the exchange price after someone sold something
  void sell(std::string name, std::string stock, int share_sold);
};
