#include "./market.hpp"

// Updates the marketfile
inline void smarket::update(std::string info) {
  this->a->add_text("~/.local/mrkt" + this->name, info);
}

// Sets the exchange price for a stock
bool smarket::set_exchange_price(std::string exchange_name, float new_price) {
  // Check if we can find the stock in the market vector
  if (exchange_prices.find(exchange_name) == exchange_prices.end()) {
    // If not, return false
    return false;
  }

  // Else update both the prev_exchange_price and exchange_price values
  this->prev_exchange_price[exchange_name] =
      this->exchange_prices[exchange_name];
  this->exchange_prices[exchange_name] = new_price;

  // The return true
  return true;
}

// Create a =n exchange
void smarket::create_exchange(std::string exchange_name, float exchange_price,
                              int total_shares) {
  this->exchanges += 1;
  // Adds the exchange to the vector
  this->exchange_vector.push_back(exchange_name);

  // Set exchange price
  this->exchange_prices[exchange_name] = exchange_price;

  // Update the mrktfile
  this->update("Exchange " + exchange_name + "With " +
               std::to_string(total_shares) + " shares" +
               " has been created with price " +
               std::to_string(exchange_price));
}

// Get all the exchnages returns a array of strings
std::string *smarket::get_exchanges() {

  // Creates a vector of exchanges
  std::vector<std::string> exchanges;

  // Takes each exchange and makes it an element in he vector
  for (int i; i < get_total_exchanges(); i++) {
    exchanges.push_back(exchange_vector[i]);
  }

  // Creates an array with 100 spaces
  std::string arr[100];

  // Copies the array from the exchanges
  std::copy(exchanges.begin(), exchanges.end(), arr);
  return arr;
}

// Get exchange price
float smarket::get_exchange_price(std::string exchange_name) {
  // Try to find the exchnage
  if (exchange_prices.find(exchange_name) == exchange_prices.end()) {
    return false;
  }

  // If can find it return the exchange price
  return this->exchange_prices[exchange_name];
}

// Get exchange share
int smarket::get_exchange_shares(std::string exchange_name) {
  // Check if the exchange is in the exchanges vector
  if (exchange_prices.find(exchange_name) == exchange_prices.end()) {
    // If not, return false
    return false;
  }

  // If it is, return the total exchange shares
  return this->exchange_amount[exchange_name];
}

// Buy a stock
void smarket::buy(std::string name, std::string stock, int share_bought) {
  // Set a new exchange price
  this->set_exchange_price(
      stock, mrkt::get_price_buy(this->exchange_total[stock],
                                 this->prev_exchange_price[name], share_bought,
                                 1, this->get_exchange_shares(stock)));

  // Adding the user the exchange
  this->exchange_users[stock].push_back(name);

  // Update the mrkt file
  this->update(name + " bought stock worth " +
               std::to_string(this->prev_exchange_price[name]) +
               ", the price is now is now " +
               std::to_string(((int)this->get_exchange_shares(stock))));
}

// Sell a stock
void smarket::sell(std::string name, std::string stock, int share_sold) {
  // Set the exchange price
  this->set_exchange_price(
      stock, mrkt::get_price_sell(this->exchange_total[stock],
                                  this->prev_exchange_price[name], share_sold,
                                  this->get_exchange_shares(stock)));
  // Update the file
  this->update(name + " sold stock worth " +
               std::to_string(this->prev_exchange_price[name]) +
               ", the price is now " +
               std::to_string(((int)this->get_exchange_shares(stock))));
}

// Gets to tal exchange
int smarket::get_total_exchanges() { return this->exchange_vector.size(); }
