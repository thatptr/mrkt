#include <fstream>
#include <string>
#include <sys/stat.h>
#include <unistd.h>

namespace mrkt {
class files {
private:
  // Checks if a file exists
  bool exists(std::string file_or_folder) {
    return (access(file_or_folder.c_str(), F_OK) != -1);
  }

public:
  bool create_file(std::string file) {
    if (exists(file))
      return false;

    std::ofstream a = std::ofstream{file};
    return true;
  }

  bool create_folder(std::string folder) {
    if (exists(folder))
      return false;

    mkdir(folder.c_str(), 0777);
    return true;
  }

  bool add_text(std::string file, std::string text) {
    if (!exists(file))
      return false;

    std::ofstream f;
    f.open(file, std::ios_base::app);
    f << text;
    return true;
  }
};
} // namespace mrkt
