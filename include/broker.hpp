// C++ Includes
#include <algorithm>

// Local includes
#include "./market.hpp"

class broker {
private:
  std::string name;
  std::unique_ptr<smarket> market;
  int capital;
  
  std::vector<std::string> exchange_vector;
  std::map<std::string, float> exchange_total;
  
  // Update a vector
  template <class T> void update_vector(T* a, size_t size_a, std::vector<T> b);
  
  // Set the capital variable
  void set_capital(int capital);
  
  template <class T> inline bool exists(T x);
  
public:
  broker(int capital, std::string name){
    this->market = std::make_unique<smarket>(capital, name);
  }
  
  // Update the exchanges
  void update();

  // Buy stock
  bool buy(std::string stock, int amount_of_shares_bought);
  
  // Sell stock
  bool sell(std::string stock, int aamount_of_shares_sold);
  
  // Get the broker's capital
  int get_capital();
  
  // Add money to capital
  void add_to_capital(int capital);

  // Remove money from capital
  void remove_from_capital(int capital);
};
