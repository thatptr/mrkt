#include "./broker.hpp"

template <class T>
void broker::update_vector(T *a, size_t size_a, std::vector<T> b) {
  // Remove items that are no longer found in array a
  b.erase(std::remove_if(b.begin(), b.end(),
                         [&](const T &item) {
                           return std::find(a, a + size_a, item) ==
                                  (a + size_a);
                         }),
          b.end());

  // Add items that are in array a but are not found in array b
  for (size_t i = 0; i < size_a; ++i) {
    if (std::find(b.begin(), b.end(), a[i]) == b.end()) {
      b.push_back(a[i]);
    }
  }
}

void broker::remove_from_capital(int capital) {
  set_capital(this->capital - capital);
}

void broker::add_to_capital(int capital) {
  set_capital(this->capital + capital);
}

int broker::get_capital() { return this->capital; }

bool broker::sell(std::string stock, int amount_of_shares_sold) {
  // Updates stock
  this->update();

  // Check if the exchange price of the stock is greater than the capital and
  // the stock exists
  if ((int)this->market->get_exchange_price(stock) < this->capital ||
      !this->exists(stock)) {
    return false;
  }

  // If true, Sell stock
  this->market->sell(this->name, stock, amount_of_shares_sold);
  return true;
}

bool broker::buy(std::string stock, int amount_of_shares_bought) {
  // Updates stock
  this->update();
  // Check if the exchange price of the stock is lesser than the capital and the
  // stock exists
  if ((int)this->market->get_exchange_price(stock) > this->capital ||
      this->exists(stock)) {
    return false;
  }

  // If true, Buy stock
  this->market->buy(this->name, stock, amount_of_shares_bought);
  return true;
}

void broker::update() {
  this->update_vector(market->get_exchanges(), sizeof(market->get_exchanges()),
                      this->exchange_vector);
}

void broker::set_capital(int capital) { this->capital = capital; }

template <class T> inline bool broker::exists(T x) {
  return std::find(std::begin(this->exchange_vector),
                   std::end(this->exchange_vector),
                   x) != std::end(this->exchange_vector);
}
