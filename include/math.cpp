#include <iostream>
namespace mrkt {
  static inline double percent(int a, int b){
    std::cout << "a: " << a << ", b: " << b << std::endl;
    return (static_cast<double>(a) / b) * 100;
  }

  inline int get_price_buy(int total_shares, int previous_price, int shares_bought, float inflation, int available_shares) {
    // Check if total_shares is 0
    if (total_shares == 0) {
      // Handle this case appropriately (e.g., return an error or set a default price)
      return previous_price + shares_bought * inflation;
    }

    // Get a percentage of the total_shares
    double percent_of_shares = percent(shares_bought, total_shares);

    int end_price = static_cast<int>((percent_of_shares * previous_price) * inflation);
    return (end_price + previous_price);
  }

  inline int get_price_sell(int total_shares, int previous_price, int shares_sold, int available_shares){
    // Get a percentage of the total_shares
    if (total_shares == 0){
      // Handle this case appropriately (e.g., return an error or set a default price)
      return previous_price - shares_sold ;
    }

    double percent_of_shares = percent(shares_sold, total_shares);

    int end_price = static_cast<int>(percent_of_shares * previous_price);
    return (previous_price - end_price);
  }

}
